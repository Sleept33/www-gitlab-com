---
layout: markdown_page
title: "Production Team"
---

## Common Links

- [Public Infrastructure Issue Tracker](https://gitlab.com/gitlab-com/infrastructure/issues/);
please use confidential issues for topics that should only be visible to team members at GitLab.
- [Chat channel](https://gitlab.slack.com/archives/production); please use the
`#production` chat channel for questions that don't seem appropriate to use the
issue tracker or the internal email address for.
- [GitLab Production Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com&ctz=America/Los_Angeles); add it to your own views by hitting the `+GoogleCalendar` button in the lower right of the screen when viewing the Calendar with the link here.

## On this page
{:.no_toc}

- TOC
{:toc}

## Production Team

Production engineers work on keeping the infrastructure that runs our services
running fast and reliably. This infrastructure includes staging, GitLab.com and
dev.GitLab.org; see the [list of nodes](https://dev.gitlab.org/cookbooks/chef-repo/tree/master/nodes).

Production engineers also have a strong focus on building the right toolsets
and automations to enable development to ship features as fast and bug free as
possible, leveraging the tools provided by GitLab.com itself - we must dogfood.

Another part of the job is building monitoring tools that allow quick
troubleshooting as a first step, then turning this into alerts to notify based on
symptoms, to then fixing the problem or automating the remediation. We can only scale
GitLab.com by being smart and using resources effectively, starting with our
own time as the main scarce resource.

### Tenets

1. Security: reduce risk to its minimum, and make the minimum explicit.
1. Transparency, clarity and directness: public and explicit by default, we work in the open, we strive to get signal over noise.
1. Efficiency: smart resource usage, we should not fix scalability problems by throwing more resources at it but by understanding where the waste is happening and then working to make it disappear. We should work hard to reduce toil to a minimum by automating all the boring work out of our way.

## Prioritizing Issues

Given the variety of responsibilities and number of "interfaces" between the Production
team and all the other teams at GitLab, here is a guideline on how to prioritize
the issues we work on. Basing this on the [goals of the Infrastructure team](../#infragoals) as
well as our [values](/handbook/values/) and [workflows](/handbook/engineering/workflow)
as a company as whole, the priority should be:

1. keeping GitLab.com available - and secure
1. unblocking others
1. automating tasks to reduce toil and increase _team_ availability (but be
  explicit about the [costs](https://xkcd.com/1319/) and [benefits](https://xkcd.com/1205/)
1. improving performance of GitLab.com while being conscious of cost
1. reducing costs of running GitLab.com

### Labeling Issues

We use [issue labels](https://gitlab.com/gitlab-com/infrastructure/labels) to
assist in organizing issues within the Infrastructure issue tracker. Prioritized labels are

- `~critical`
- `~security`
- `~chef`
- `~noise`
- `~blocked`

Issues in this tracker should be organized into [boards](https://gitlab.com/gitlab-com/infrastructure/boards)
 and [milestones](https://gitlab.com/gitlab-com/infrastructure/milestones) to define projects and timelines respectively.

## Production events logging

There are 2 kind of production events that we track:

- Changes to the production fleet: for this we record things [in the Chef Repo](https://dev.gitlab.org/cookbooks/chef-repo).
  - Deploys will be recorded automagically because of the way we do deploys.
  - General operations can be recorded by creating an empty commit in the repo and pushing it into origin.
- Outages and general production incidents
  - If we are required to act in production manually to perform any operation we should create an issue and consider labeling it as _toil_ to track the cost of such manual work load.
  - It we had a disruption in the service, we must create a blameless post mortem. Refer to the [Outages and Blameless Post Mortems](../#postmortems) section of the Infrastructure page.
