---
layout: markdown_page
title: "GitLab Offboarding"
---

Before starting an offboarding issue, make sure that the team member's resignation or termination has been discussed and cleared with _at least_ the member of the executive team to whom the team member (in)directly reports.
For [involuntary terminations](/handbook/people-operations/#involuntary-terminations), make yourself familiar with the guidelines in the People Operations handbook.

When it is time for offboarding, [create a new **confidential** issue](https://gitlab.com/gitlab-com/peopleops/issues/new) for former team member using the `offboarding` [template](https://gitlab.com/gitlab-com/peopleops/blob/master/.gitlab/issue_templates/offboarding.md) using the [dropdown](https://docs.gitlab.com/ce/user/project/description_templates.html#using-the-templates), and edit it for applicability to the individual.
Please [update the checklist](https://gitlab.com/gitlab-com/peopleops/edit/master/.gitlab/issue_templates/offboarding.md) as more steps arise.

## Managing the Offboarding Tasks

- **Remove a team member from Sundial**<a name="sundial-removal"></a>
1. Log in to Sundial using the link in the "Private Sundial URL" Google Doc  
1. Go to timezone view in the top right corner
1. Hover over the team member's name that you want to remove
1. Make sure you are in list view at this point, and then click the "x" to remove the team member

### Returning property to GitLab
{: #returning-property}

As part of offboarding, any GitLab property needs to be returned to GitLab. GitLab will pay for the shipping either by People Ops sending a FedEx shipping slip or it can be returned by another mutually agreed method. If property is not returned, GitLab reserves the right to use a creditor company to help retrieve the property.

#### FedEx info for the team member

Pull up the departing contributors home address. Find a Fedex location close to them. Then follow the template below.

"Hi [Name of team member]

The closest Fedex to you is located on [full street address, city, state, zip code]. Driving or walking directions can be found here: [enter link from “mapping home location to the Fedex location”]

Once there you can use Fedex boxes that you need and do the following actions:

1. Package the equipment in the box
2. Send package to GitLab HQ in San Francisco
3. Mark on the invoice "Bill Recipient"
4. Our Fedex number is: [find the number in Secretarial Vault in 1Password]
5. Provide People Ops the tracking number via email

Please let People Ops know if you have any questions."
